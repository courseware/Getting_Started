<b>Table of Contents</b>
1. [Getting Started](#getting-started)<br/>
a. [Create a GitLab Account](#create-a-gitlab-account)<br/>
b. [Access Courseware](#access-courseware)<br/>
c. [New Courseware Request](#new-courseware-request)<br/>
d. [Edit Courseware Request](#edit-courseware-request)<br/>

2. [Working with GitKraken](#working-with-gitkraken)<br/>
a. [Cloning](#cloning)<br/>
b. [Branching](#branching)<br/>
c. [Staging](#staging)<br/>
d. [Commit](#commit)<br/>
e. [Push](#push)<br/>
f. [Merge Request](#merge-request)<br/>


# Getting Started
GitLab has been chosen to store courseware and related documentation so that versions, 
issues, milestones and permissions can be tracked.  GitLab also lets us integrate
quality through the GitLab Workflow's "merge request" feature.  This ensures that
courseware developers and editors are working on their own branch or copy of the 
repository and then submit a merge request to reviewers to approve before merging
back into the master branch, or trunk.

We will be using the [Feature Branch Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows#feature-branch-workflow).
Please review and understand the process flow.

Also, we require that you use Scrum Board, similar to Trello, to manage your tasks in creating or editing courseware.  This tool is a way for us to communicate progress, build quality into the development or editing of the courseware, and ensure timely completion in order to allow enough time for the print process.  Please communicate your choice of tools and a link to the board to [courseware@ntiertraining.com](mailto:courseware@ntiertraining.com).

## Create a GitLab Account
* [Sign up](https://gitlab.com/users/sign_in) for a GitLab account if you don't have one.

## Access Courseware
1. Email [access_courseware@ntiertraining.com](mailto:access_courseware@ntiertraining.com) to request access to view courseware along
    with GitLab user id.
2. You will receive an email with non-disclosure forms to sign and send back.
3. When the non-disclosure form is signed, you will receive an email with the
    URL of the course.

## New Courseware Request
1. Email [new_courseware@ntiertraining.com](mailto:new_courseware@ntiertraining.com) to request the creation of new courseware
    along with GitLab user id.
2. Download [GitKraken](https://www.gitkraken.com).
3. Open GitKraken and create a GitKraken account with your profile information.
4. You will receive an email when the project has been setup with contract forms 
    to sign and an estimate to send back.
5. When the contracts are signed and estimates submitted, you will receive an 
    email on configuring GitKraken to the new project.
6. Setup GitKraken for your project according to email instructions.
7. Request access to [Design Guideleines](https://gitlab.com/courseware/Design_Guidelines), [Startup](https://gitlab.com/courseware/Startup), and [Templates](https://gitlab.com/courseware/Templates).
8. Download the [Startup](https://gitlab.com/courseware/Startup) project.
9. Follow [guidelines](https://gitlab.com/courseware/Design_Guidelines) and use [templates](https://gitlab.com/courseware/Templates) for course creation.
10. Stage/Commit/Push frequently to your branch while you are creating the courseware.
11. Submit a Pull Request when complete and ready for review.

## Edit Courseware Request
1. Email [edit_courseware@ntiertraining.com](mailto:edit_courseware@ntiertraining.com) to request editting of existing courseware
    along with GitLab user id.
2. Download [GitKraken](https://www.gitkraken.com).
3. Open GitKraken and create a GitKraken account with your profile information.
4. You will receive an email with contract forms to sign and an estimate to send back.
5. When the contracts are signed and estimates submitted, you will receive an 
    email on configuring GitKraken to the existing project.
6. Setup GitKraken for your project according to email instructions.
7. Follow [guidelines](https://gitlab.com/courseware/Design_Guidelines) and use [templates](https://gitlab.com/courseware/Templates) for course creation.
8. Stage/Commit/Push frequently to your branch while you are creating the courseware.
9. Submit a Pull Request when complete and ready for review.

# Working with GitKraken
Following is a Quick Start guide to using GitKraken with GitLab.  To read a more comprehensive document on using GitKraken and all of its features, go to [GitKraken's Documentation](https://support.gitkraken.com/getting-started/guide).
<br/><br/>
## Cloning
1. The first thing you will do is clone the repository that was sent to you in the email.
<br/><br/>
<img src="http://gitlab.com/courseware/Getting_Started/raw/master/images/GitKraken_Clone1.png" width="400px" alt="Clone image" style="margin:20px;">
<br/><br/>
2. Configure the repository in GitKraken by adding the location you want the repository to reside on your machine under the URL option.
<br/><br/>
<img src="http://gitlab.com/courseware/Getting_Started/raw/master/images/GitKraken_Clone2.png" width="600px" alt="URL image" style="margin:20px;">
<br/><br/>
3. And finally copying and pasting the GitLab Project repo URL into GitKraken.
<br/><br/>
<img src="http://gitlab.com/courseware/Getting_Started/raw/master/images/GitLab_Repo_URL.png" width="600px" alt="GitLab copy image" style="margin:20px;">
<br/><br/><br/>

## Branching
We don't want to work directly off of the 'master' branch as this wouldn't allow us to ensure the quality of the course materials.  So we create a branch from the 'master' branch and give it a name describing the feature or what we are going to do.  To create a branch in GitKraken, just tap the Branch button at the middle top, and give it a name.  You will notice under the 'Local' section on the right that you should see your new branch along with the 'master' branch.
<br/><br/>
<img src="http://gitlab.com/courseware/Getting_Started/raw/master/images/Branching.png" width="600px" alt="Branching image" style="margin:20px;">
<br/><br/>
Notice the new branch, 'ember_courseware', that has been created and is now set to make changes on.
<br/><br/><br/>
## Staging
Now that you have the repository onto your machine you can begin editting and 
adding new files.  In order for added files to be added to the repository, you
must stage them so they can be committed.  Here is how you stage new files.
<br/><br/>
When you add new files, you should see a WIP line above the last commit and when 
you click or tap on the WIP, you willl see a column on the right where you can
stage the new file(s).
<br/><br/>
<img src="http://gitlab.com/courseware/Getting_Started/raw/master/images/Staging.png" width="800px" alt="Staging image" style="margin:20px;">
<br/><br/>
Click the "Stage All Changes" button to stage changes to be committed.
<br/><br/><br/>

## Commit
Now that your new files have been staged, or you have made changes to existing files.  You are ready to commit your changes to your local repository.  To commit your changes, additions or deletions, type in a message in the "Commit Message" area at the bottom of the right column describing what you have done, and click the "Commit" button.
<br/><br/>
<img src="http://gitlab.com/courseware/Getting_Started/raw/master/images/Commit.png" width="800px" alt="Commit image" style="margin:20px;">
<br/><br/><br/>

## Push
Once your files have been committed locally, you will want to push your changes to the server, since having them only on your machine is risky.  Being that you don't have a remote branch created, when you push your changes to the remote server, it will create it for you.  You will see it added in the left column under the 'Remote' section.
<br/><br/>
<img src="http://gitlab.com/courseware/Getting_Started/raw/master/images/Push.png" width="800px" alt="Push image" style="margin:20px;">
<br/><br/><br/>

## Merge Request
When you are ready for your work to be reviewed and ultimately merged or brought into the 'master' branch, you will submit a 'Merge Request' on your branch in GitLab.  Navigate to the 'Repository' under the Project you are working in, click on 'Branches', and you will see a button to the right of your branch titled, 'Merge Request'.
<br/><br/>
<img src="http://gitlab.com/courseware/Getting_Started/raw/master/images/Merge_Request.png" width="800px" alt="Push image" style="margin:20px;">
<br/><br/>
Click the button and fill out the form.  Be sure to add "WIP: " before the title as is mentioned until the assignee of the merge request has given the go ahead to not include it.  You will be given instructions on who to assign to the request and what milestone to attach it to.  From here, Issues will be open in GitLab and assigned to you for any changes still needing to be made.  Make your changes, commit them, push them to the remote branch.  After your changes have been reviewed, then the merge request will be accepted and closed.